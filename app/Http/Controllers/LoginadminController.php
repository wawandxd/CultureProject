<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;
use DB;

class LoginadminController extends Controller
{
    public function login()
    {
        return view('adminpage.loginadmin');
    }

    public function reset()
    {
        return view('adminpage.resetpassadmin');
    }
}
