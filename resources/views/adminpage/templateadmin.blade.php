<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>.:: Administrator System | Dashboard ::.</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/select2/dist/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') }}">
  <link href="{{ asset('css/sweetalert/sweetalert2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/sweetalert/sweetalert2.css') }}" rel="stylesheet">

  @yield('custom_css')

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="" class="logo">
      <span class="logo-mini"><b>IC</b></span>
      <span class="logo-lg"><b>Indonesia</b>Culture</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		    <li class="dropdown messages-menu">
            <a href="" target="_blank" title="Go To Web Public">
              <i class="fa fa-globe"></i>
            </a>
         </li>
         <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-warning">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 0 messages</li>
              <li>
                <ul class="menu">

                </ul>
              </li>
              <li class="footer"><a href="">See All Messages</a></li>
            </ul>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('storage/app/public/useruploads/') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">aaaaaaaa</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="{{ asset('storage/app/public/useruploads/') }}" class="img-circle" alt="User Image">
                <p>
                  dfdfhfdhdfh
                  <small>Member since dfhdfhdfhfdh</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                      <a id="out" class="btn btn-default btn-flat" customParam="" href="">Sign Out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('storage/app/public/useruploads/') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>gfhfgdhfghdhfdh</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      @include('adminpage.menunav')
    </section>
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
    </section>

       @yield('content')

  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright © 2017. Indonesia Culture 2017 Information System</strong> | All rights
    reserved.
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<script src="{{ asset('template/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ asset('template/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('template/admin/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('template/admin/bower_components/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('template/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('template/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('template/admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
  })
</script>
<!-- Script SweetAlert Konfirmasi Hapus To Trash-->
<script>
    var deleter = {

        linkSelector : "a#out",

        init: function() {
            $(this.linkSelector).on('click', {self:this}, this.handleClick);
        },

        handleClick: function(event) {
            event.preventDefault();

            var self = event.data.self;
            var link = $(this);

        swal({
              title: "Sign Out",
              text: "Are You Sure To Sign Out From This Session ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, Sign Out!",
              cancelButtonText: "No, cancel it!",
              closeOnConfirm: false,
              closeOnCancel: false
          }).then(function () {
              window.location = link.attr('customParam');
          }, function (dismiss) {
            if (dismiss === 'cancel') {
              swal(
                'Cancel',
                'Sign Out Session Is Canceled',
                'error'
              )
            }
          })
        },
    };

    deleter.init();
</script>
<!-- Script SweetAlert Konfirmasi Hapus -->
<script>
  $(function () {
    CKEDITOR.replace('editor1')
    $('.textarea').wysihtml5()
  })

  $(function () {
    CKEDITOR.replace('editor2')
    $('.textarea').wysihtml5()
  })
</script>
<script>
  $(document).ready(function() {
    $(".select2_group").select2({});
    $(".select2_multiple").select2({
      maximumSelectionLength: 0,
      placeholder: "Silahkan pilih kategori yang sesuai",
      allowClear: true
    });
  });
</script>
<!-- Sweet Alert -->
<script src="{{ asset('js/sweetalert/sweetalert2.js') }}"></script>

@include('sweet::alert')

@yield('custom_script')

</body>
</html>
