@extends('adminpage.templateadmin')

@section('content')
    <section class="content">
      <div class="row">
        <section class="col-lg-12">
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-tachometer"></i>
              <h3 class="box-title"><b>WELCOME :</b> </h3>
            </div>
            <div class="box-body">
				           Selamat Datang <br><br>
                   Hai Administrator , selamat datang di halaman Administrator.<br>
                   Silahkan klik menu pilihan yang berada di sebelah kiri untuk mengelola konten website anda.<br>
            </div>
            <div class="box-footer clearfix">
                  <p align="right">
                   <?php
                      date_default_timezone_set("Asia/Jakarta");
                      $today = date("j F Y | h:i:s a");
                      echo "Tanggal : $today"
                   ?>
                 </p>
            </div>
          </div>
        </section>
      </div>
  </section>

@include('sweet::alert')

@endsection
